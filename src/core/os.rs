//! Forward calls and wraps any error into common enum which carry problematic path.
//! May not expose all parameters of underlying functions.

// Standard paths
use std::ffi::CString;
use std::fs;
use std::fs::OpenOptions;
use std::io::Read;
use std::io::Write;
use std::os::unix;

// External paths
use nix::kmod;
use nix::mount;

// Crate paths
use error::OsError;

// Crate modules
pub mod error;

pub struct FileHandle {
    path: String,
    fd: fs::File,
}

/// Open file in read mode.
///
/// # Errors
///
/// * see [fs::File::open].
pub fn open_read(path: &str) -> Result<FileHandle, OsError> {
    let fd = fs::File::open(path).map_err(|source| OsError::OpenFile {
        path: path.to_string(),
        source,
    })?;

    Ok(FileHandle {
        path: path.to_string(),
        fd,
    })
}

/// Open file in read and write mode.
/// It truncates existing file.
///
/// # Errors
///
/// * see [OpenOptions::open].
pub fn open_read_write(path: &str) -> Result<FileHandle, OsError> {
    let fd = OpenOptions::new()
        .read(true)
        .write(true)
        .open(path)
        .map_err(|source| OsError::OpenFile {
            path: path.to_string(),
            source,
        })?;

    Ok(FileHandle {
        path: path.to_string(),
        fd,
    })
}

/// Write data to file.
///
/// # Errors
///
/// * see [fs::write].
pub fn write(path: &str, data: &str) -> Result<(), OsError> {
    fs::write(path, data).map_err(|source| OsError::Write {
        path: path.to_string(),
        source,
    })
}

/// Read data from file as string
///
/// # Errors:
///
/// * see [fs::read_to_string]
pub fn read_to_string(path: &str) -> Result<String, OsError> {
    fs::read_to_string(path).map_err(|source| OsError::Read {
        path: path.to_string(),
        source,
    })
}

/// Write to file handle.
///
/// # Errors
///
/// * see [std::io::Write::write].
pub fn write_file_handle(fh: &mut FileHandle, data: &[u8]) -> Result<usize, OsError> {
    fh.fd.write(data).map_err(|source| OsError::Write {
        path: fh.path.clone(),
        source,
    })
}

/// Read from file handle.
///
/// # Errors
///
/// * see [std::io::Read::read].
pub fn read_file_handle(fh: &mut FileHandle, data: &mut [u8]) -> Result<usize, OsError> {
    fh.fd.read(data).map_err(|source| OsError::Read {
        path: fh.path.clone(),
        source,
    })
}

/// Remove file.
///
/// # Errors
///
/// * see [fs::remove_file].
pub fn remove_file(path: &str) -> Result<(), OsError> {
    fs::remove_file(path).map_err(|source| OsError::RemoveFile {
        path: path.to_string(),
        source,
    })
}

/// Create directories recursively.
///
/// # Errors
///
/// * see [fs::create_dir_all].
pub fn create_dir_all(path: &str) -> Result<(), OsError> {
    fs::create_dir_all(path).map_err(|source| OsError::CreateDirectories {
        path: path.to_string(),
        source,
    })
}

/// Read directory.
///
/// # Errors
///
/// * see [fs::read_dir].
pub fn read_dir(path: &str) -> Result<fs::ReadDir, OsError> {
    fs::read_dir(path).map_err(|source| OsError::ReadDirectory {
        path: path.to_string(),
        source,
    })
}

/// Remove directory.
///
/// # Errors
///
/// * see [fs::remove_dir].
pub fn remove_dir(path: &str) -> Result<(), OsError> {
    fs::remove_dir(path).map_err(|source| OsError::RemoveDirectory {
        path: path.to_string(),
        source,
    })
}

/// Create symbolic link.
///
/// # Errors
///
/// * see [unix::fs::symlink].
pub fn symlink(orginal: &str, link: &str) -> Result<(), OsError> {
    unix::fs::symlink(orginal, link).map_err(|source| {
        OsError::CreateSymlinkUsbFunctionForConfiguration {
            orginal: orginal.to_string(),
            link: link.to_string(),
            source,
        }
    })
}

/// Load kernel module.
///
/// # Errors
///
/// * see [kmod::finit_module].
pub fn finit_module(path: &str) -> Result<(), OsError> {
    let module = open_read(path)?;

    // Creating CString from empty str is always fine
    let param_values = CString::new("").unwrap();

    kmod::finit_module(&module.fd, &param_values, kmod::ModuleInitFlags::empty()).map_err(
        |source| OsError::LoadKernelModuleGadget {
            source: source.into(),
        },
    )
}

/// Mount provided function filesystem.
///
/// # Errors
///
/// * see [mount::mount].
pub fn mount_functionfs(source: &str, target: &str) -> Result<(), OsError> {
    const NONE: Option<&'static [u8]> = None;

    mount::mount(
        Some(source),
        target,
        Some(b"functionfs".as_ref()),
        mount::MsFlags::empty(),
        NONE,
    )
    .map_err(|source| OsError::MountFunctionFS {
        device: source.to_string(),
        target: target.to_string(),
        source: source.into(),
    })
}

/// Unmount provided filesystem.
///
/// # Errors
///
/// * see [mount::umount].
pub fn umount(path: &str) -> Result<(), OsError> {
    mount::umount(path).map_err(|source| OsError::Unmount {
        path: path.to_string(),
        source: source.into(),
    })
}
