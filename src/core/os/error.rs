// Standard paths
use std::io;

// External paths
use thiserror::Error;

#[derive(Error, Debug)]
pub enum OsError {
    #[error("OsError: cannot open file: path {path}")]
    OpenFile { path: String, source: io::Error },
    #[error("OsError: cannot load kernel module gadget")]
    LoadKernelModuleGadget { source: io::Error },
    #[error("OsError: cannot read directory: path: {path}")]
    ReadDirectory { path: String, source: io::Error },
    #[error("OsError: cannot remove file: path: {path}")]
    RemoveFile { path: String, source: io::Error },
    #[error("OsError: cannot remove directory: path: {path}")]
    RemoveDirectory { path: String, source: io::Error },
    #[error("OsError: cannot create directories: path: {path}")]
    CreateDirectories { path: String, source: io::Error },
    #[error("OsError: cannot write to file: path: {path}")]
    Write { path: String, source: io::Error },
    #[error("OsError: cannot read from file: path: {path}")]
    Read { path: String, source: io::Error },
    #[error("OsError: cannot create symbolic link: orginal: {orginal} link: {link}")]
    CreateSymlinkUsbFunctionForConfiguration {
        orginal: String,
        link: String,
        source: io::Error,
    },
    #[error("OsError: cannot mount functionfs: device: {device} target: {target}")]
    MountFunctionFS {
        device: String,
        target: String,
        source: io::Error,
    },
    #[error("OsError: cannot unmount filesystem: path: {path}")]
    Unmount { path: String, source: io::Error },
}
