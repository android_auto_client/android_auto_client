// Standard paths
use std::io;

// External paths
use thiserror::Error;

// Crate paths
use crate::core::os::error::OsError;
use crate::core::usb::gadget::initial::error::UsbGadgetInitialCleanError;
use crate::core::usb::gadget::initial::error::UsbGadgetInitialSetupError;
use crate::core::usb::gadget::main::error::UsbGadgetMainCleanError;
use crate::core::usb::gadget::main::error::UsbGadgetMainSetupError;

#[derive(Error, Debug)]
pub enum UsbError {
    #[error("UsbError: cannot read running linux kernel version")]
    UnknownKernelVersion { source: io::Error },
    #[error("UsbError: cannot covert kernel version to utf8 charset")]
    NotUtf8KernelVersion,
    #[error("UsbError: cannot load kernel module gadget")]
    LoadKernelModuleGadget { source: OsError },
    #[error("UsbError: cannot access usb gadget controller kernel subsystem")]
    UsbGadgetControllerKernelSubsystemNotAvailable { source: OsError },
    #[error("UsbError: cannot find any usb gadget controller")]
    NotAnyUsbGadgetController,
    #[error("UsbError: cannot iterate over usb gadget controller")]
    AccessUsbGadgetController { source: io::Error },
    #[error("UsbError: usb gadget initial clean error")]
    UsbGadgetInitialCleanError {
        #[from]
        source: UsbGadgetInitialCleanError,
    },
    #[error("UsbError: usb gadget initial setup error")]
    UsbGadgetInitialSetupError {
        #[from]
        source: UsbGadgetInitialSetupError,
    },
    #[error("UsbError: usb gadget main clean error")]
    UsbGadgetMainCleanError {
        #[from]
        source: UsbGadgetMainCleanError,
    },
    #[error("UsbError: usb gadget main setup error")]
    UsbGadgetMainlSetupError {
        #[from]
        source: UsbGadgetMainSetupError,
    },
}
