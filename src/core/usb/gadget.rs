//! Setup or clean usb gadgets.
//!
//! In initial phase Android Auto client gadget introduce itself as mass storage cdrom.

// External paths
use log::debug;

// Crate modules
use crate::core::usb::error::UsbError;

// Declare modules
pub mod initial;
pub mod main;

/// Clean any previously setup gadget.
///
/// # Errors
///
/// * cannot clean gadget.
pub fn clean() -> Result<(), UsbError> {
    debug!("cleaning previously registered related gadgets if any");

    initial::clean()?;
    main::clean()?;

    Ok(())
}

/// Firstly setup initial gadget.
/// It itroduce itself as cdrom.
/// Then wait for some special messages from head unit to detect if it supports Android Auto protocol.
/// If yes, then clean initial gadget and setup main one.
///
/// # Errors
///
/// * cannot setup gadget.
pub fn setup(controller_usb_gadget: &str) -> Result<(), UsbError> {
    debug!("setting up gadgets");

    // Firstly setup initial gadget.
    // It itroduce itself as cdrom.
    // Then wait for some special messages from head unit to detect if it supports Android Auto protocol.
    initial::setup(controller_usb_gadget)?;

    // Android Auto protocol is detected correctly using initial gadget
    // Now clean this inititial gadget before entering main gadget
    initial::clean()?;

    main::setup(controller_usb_gadget)?;

    Ok(())
}
