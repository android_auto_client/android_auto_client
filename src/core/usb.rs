//! Setup Android Auto client as usb gadget device.
//!
//! # Documentation
//!
//! * [Kernel USB Gadget Configfs Interface](https://elinux.org/images/e/ef/USB_Gadget_Configfs_API_0.pdf)
//! * [gadget configfs](https://www.kernel.org/doc/html/latest/usb/gadget_configfs.html)
//! * [gadget testing](https://www.kernel.org/doc/html/latest/usb/gadget-testing.html)
//! * [gadget functionfs](https://www.collabora.com/news-and-blog/blog/2019/03/27/modern-usb-gadget-on-linux-and-how-to-integrate-it-with-systemd-part-2/)
//! * [gadget performance](https://elinux.org/images/a/ae/Ott--usb_and_the_real_world.pdf)

// Standard paths
use std::path::Path;

// External paths
use log::debug;
use log::info;
use nix::sys::utsname;

// Crate paths
use crate::core::os;
use error::UsbError;

// Crate module
pub mod error;
mod gadget;

/// Load kernel module libcomposite to expose gadget api.
///
/// # Errors
///
/// * cannot read running kernel version
/// * cannot covert running kernel version into utf8 charset
/// * cannot find kernel module libcomposite. Ensure that kernel is configured to build this module.
/// * cannot load kernel module libcomposite. Ensure you have rights for privilaged action.
fn kernel_module_gadget_load() -> Result<(), UsbError> {
    debug!("loading gadget kernel module");
    let uname = utsname::uname().map_err(|source| UsbError::UnknownKernelVersion {
        source: source.into(),
    })?;
    let version_kernel_running = uname
        .release()
        .to_str()
        .ok_or(UsbError::NotUtf8KernelVersion {})?;

    let module = format!(
        "/lib/modules/{version_kernel_running}/kernel/drivers/usb/gadget/libcomposite.ko",
        version_kernel_running = version_kernel_running
    );

    os::finit_module(&module).map_err(|source| UsbError::LoadKernelModuleGadget { source })?;

    info!("loaded gadget kernel module");

    Ok(())
}

/// Check if kernel gadget api is already exposed and if not run function to load related module.
///
/// # Errors
///
/// * see [kernel_module_gadget_load].
fn kernel_module_gadget_ensure_loaded() -> Result<(), UsbError> {
    debug!("checking if gadget kernel module is already loaded");

    if !Path::new("/sys/kernel/config/usb_gadget").is_dir() {
        kernel_module_gadget_load()
    } else {
        Ok(())
    }
}

/// Find physical address location of usb gadget controller.
///
/// Assume there is only one usb gadget controller available in system.
///
/// # Errors
///
/// * cannot find any usb gadget controller. Ensure running hardware has such and kernel is configured to support it.
/// * cannot access usb gadget controller. Ensure you have rights for privilaged axtion.
fn gadget_usb_controller_get() -> Result<String, UsbError> {
    debug!("finding usb gadget controller");

    let mut controllers_usb_gadget = os::read_dir("/sys/class/udc")
        .map_err(|source| UsbError::UsbGadgetControllerKernelSubsystemNotAvailable { source })?;

    match controllers_usb_gadget.next() {
        Some(controller_usb_gadget) => {
            let controller_usb_gadget = controller_usb_gadget
                .map_err(|source| UsbError::AccessUsbGadgetController { source })?;

            let controller_usb_gadget = controller_usb_gadget.path();

            // Structure in sysfs is maintained by kernel
            // So path will always be file as expected
            // Then it will never panic
            let controller_usb_gadget = controller_usb_gadget.file_name().unwrap();

            // There are only ascii characters in sysfs
            // So conversion from os string to utf will never panic
            let controller_usb_gadget = controller_usb_gadget.to_str().unwrap();

            let controller_usb_gadget = String::from(controller_usb_gadget);

            Ok(controller_usb_gadget)
        }
        None => Err(UsbError::NotAnyUsbGadgetController),
    }
}

/// Setup usb communication
///
/// # Errors
///
/// * see [kernel_module_gadget_ensure_loaded].
/// * see [gadget::clean].
/// * see [gadget::setup].
pub fn setup() -> Result<(), UsbError> {
    kernel_module_gadget_ensure_loaded()?;

    let controller_usb_gadget = gadget_usb_controller_get()?;

    gadget::clean()?;

    gadget::setup(&controller_usb_gadget)?;

    Ok(())
}
