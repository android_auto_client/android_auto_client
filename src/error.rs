// External paths
use thiserror::Error;

// Crate paths
use crate::core::usb::error::UsbError;

#[derive(Error, Debug)]
pub enum AndroidAutoError {
    #[error("AndroidAutoError: usb error")]
    UsbError {
        #[from]
        source: UsbError,
    },
}
