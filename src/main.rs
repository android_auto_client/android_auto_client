#![forbid(unsafe_code)]

// Standard paths
use std::process;

// External paths
use env_logger::Env;
use log::{error, info};

// Crate paths
use android_auto_client::run;

// Program entrypoint
fn main() {
    // Initialize logger with default level info
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    // Run main logic and handle possible error
    if let Err(err) = run() {
        // Construct anyhow Error type from std error.
        // It has better formatting and all shows all chained erreor when printed.
        let err = anyhow::Error::new(err);

        // Log error
        error!("{:?}", err);

        // Exit with failure
        process::exit(1);
    }

    info!("Finished ok");
}
