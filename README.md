# Purpose

Access car screen, speakers and keyboard using Android Auto protocol.


# Hardware

Tested on raspberry pi 4.


# Dependencies

Install rust compiler and `cargo` build system
``` sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
```
Then run new shell to source new environment variables with new `PATH`.

To build crossplatform additionally
``` sh
# Install rust cross toolchain for raspberry pi 4
rustup target add arm-unknown-linux-gnueabihf

# Linker for rust cross compiler
apt-get install --yes gcc-arm-linux-gnueabihf
```


# Build

## Native

To build on native rpi4 host
``` sh
./build
```

Output binary is in path `./target/debug/android-auto-client`

## Crossplatform

To build on your pc for raspbery pi 4
``` sh
./build_cross_rpi-4
```

Output binary is in path `./target/arm-unknown-linux-gnueabihf/debug/android-auto-client`


**NOTE:** Default build profiles are `development` ones with lots of debug symbols and not optimized.


# Provision

Raspberry pi requires some configuration to allow usb port to act in on the go
mode.

Access first boot partition on raspberry pi sd card and edit `config.txt` file to add lines

``` sh
# enable usb on the go to act rpi as usb device
dtoverlay=dwc2
```
[more info](https://www.raspberrypi.com/documentation/computers/configuration.html)

Plug usb cable into rpi micro usb port labelled `usb`. It gives power and allow rpi
to act as usb device. Other end of cable should go to auto usb port.

# Run

Deploy binary on rpi and run it as root
``` sh
sudo ./android-auto-client
```

To have more logs set environment variable `RUST_LOG` to `debug` or even `trace`
``` sh
sudo RUST_LOG=debug ./android-auto-client
```


# Licenses

Project is under [GPL3 or later](https://spdx.org/licenses/GPL-3.0-or-later.html).

Used external resources which are not managed by `cargo` build system:
- [logo](https://www.pngrepo.com/svg/124835/car) is [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)


# References

- [AACS](https://github.com/tomasz-grobelny/AACS): reference open source
implementation written in cpp
- [OpenAuto](https://github.com/opencardev/openauto): server side implementation
  of Android Auto protocol to act as head unit
